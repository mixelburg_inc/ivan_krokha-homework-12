#include "MessageSender.h"
#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>

bool MessageSender::isMessage() const
{
	return !_msgQueue.empty();
}

bool MessageSender::isConnected(User& user) const
{
	return _connectedUsers.find(user) != _connectedUsers.end();
}

void MessageSender::connectUser(User& user)
{
	if (isConnected(user)) throw UserIsAlreadyConnectedException();
	_connectedUsers.insert(user);
	user.changeStatus();
}

void MessageSender::connectUser(std::string& userName)
{
	User newUser(userName);
	connectUser(newUser);
}

void MessageSender::connectUser()
{
	auto name = MyUtil::getString(_enterNameMsg);
	connectUser(name);
}


void MessageSender::removeUser(User& user)
{
	if (!isConnected(user)) throw UserIsNotConnectedException();
	_connectedUsers.erase(user);
	user.changeStatus();
}

void MessageSender::removeUser(std::string& username)
{
	User newUser(username);
	removeUser(newUser);
}

void MessageSender::removeUser()
{
	auto name = MyUtil::getString(_enterNameMsg);
	removeUser(name);
}

void MessageSender::printConnected() const
{
	int num = 0;
	for (const auto& user : _connectedUsers)
	{
		std::cout << num++ << ": " << user.getName() << std::endl;
	}
}

void MessageSender::clearFile() const
{
	std::ofstream ofs;
	ofs.open(IN_FILE, std::ofstream::out | std::ofstream::trunc);
	if (!ofs.is_open()) throw OpenFileException(IN_FILE);
	ofs.close();
}

void MessageSender::readFile() 
{
	while (true)
	{
		std::ifstream inFile(this->IN_FILE);

		if (!inFile.is_open()) throw OpenFileException(IN_FILE);

		std::string line;
		while (std::getline(inFile, line)) // read file by line
		{
			{
				std::lock_guard<std::mutex> lock(_msgQueueMutex);
				_msgQueue.push(line);
				std::cout << "Read: " << line << std::endl;
				std::cout << "There are " << _msgQueue.size() << " messages" << std::endl;
			} 
			_messageReadyCV.notify_one();
		}	
		clearFile(); // flush file contents
		inFile.close();
		
		std::this_thread::sleep_for(std::chrono::seconds(this->INTERVAL));
	}
}

void MessageSender::sendMessages()
{
	while (true)
	{
		std::ofstream outfile;
		outfile.open(OUT_FILE, std::ios_base::app);

		if (!outfile.is_open()) throw OpenFileException(OUT_FILE);

		{
			std::unique_lock<std::mutex> lock(_msgQueueMutex); // setup lock
			_messageReadyCV.wait(lock, [this] { return isMessage(); }); // wait until called
			while (!_msgQueue.empty())
			{
				for (const auto& user : _connectedUsers) // iterate throw users
				{
					outfile << user.getName() << " : " << _msgQueue.front() << std::endl;
					std::cout << "wrote " << user.getName() << " : " << _msgQueue.front() << std::endl;
				}
				_msgQueue.pop();
			}
		}

		outfile.close();
	}
}

void MessageSender::run()
{
	std::thread t(&MessageSender::readFile, this);
	std::thread t1(&MessageSender::sendMessages, this);
	t.join();
	t1.join();
}
