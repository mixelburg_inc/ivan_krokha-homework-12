#include "Table.h"

#include <vector>

int Philosoph::getIndex() const
{
	return _index;
}

void Philosoph::setIndex(const int i)
{
	_index = i;
}

bool Philosoph::isHungry() const
{
	return _hunger > 0;
}

int Table::getSecondIndex(const int lIndex)
{
	{
		int rIndex = lIndex + 1;
		if (rIndex > 4) rIndex = 0;
		return rIndex;
	}
}

void Table::eat(Philosoph& philosoph)
{
	while (philosoph.isHungry())
	{
		if (checkEat(philosoph))
		{
			{
				std::lock_guard<std::mutex> lock(_eatMutex);
				philosoph.eat();
			}
		}
	}
	std::cout << "DONE: " << philosoph.getIndex() << std::endl;
}

void Table::run()
{
	Philosoph philosophs[5];

	int i = 0;
	for (auto& p : philosophs)
	{
		p.setIndex(i);
		i++;
	}

	std::vector<std::thread> threads;
	for (auto& p : philosophs)
	{
		std::thread t(&Table::eat, this, std::ref(p));
		threads.push_back(std::move(t));
	}

	for (auto& t :threads)
	{
		t.join();
	}
}

bool Table::checkEat(const int index)
{
	return _sticks[index] && _sticks[getSecondIndex(index)];
}

bool Table::checkEat(const Philosoph& philosoph)
{
	return checkEat(philosoph.getIndex());
}
